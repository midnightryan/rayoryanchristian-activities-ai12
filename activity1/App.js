import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { TouchableOpacity, StyleSheet, Text, TextInput, View, Button, SafeAreaView } from 'react-native';

export default function App() {
  const [text, onChangeText] = React.useState(null);
  let input = text;

  return (
    <SafeAreaView style={styles.container}>
      <TextInput 
      style={styles.userInput}
      onChangeText={onChangeText}
      value={text}
      placeholder="Enter Text"
      keyboardType='default'
      />
      <Button title="Enter" onPress={() => alert(input)}/>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'darkorange',
    alignItems: 'center',
    justifyContent: 'center',
  },
    userInput: {
      fontStyle: 'normal',
      width: 380,
      height: 50,
      margin: 20,
      borderWidth: 1,
      padding: 10,
    },
});
